**Django Challenge Documentation:**

Write a simple application that allows a user to manage subscriptions using Stripe and Python Django 2.

**Functional Flow :**

**1.Old Users can Login to the application and New Users registration are allowed to register.**

**2.Once the user registration is through :**

* A User profile will be created in Django with attributes 

	- Username 

	- StartDate - Subscription start date

	- Free\_trial = 7

    - Free\_trialEndDate = Subscription start date + free trial

* Also the Customer would be created on Stripe and assigned with a membership of $49.99 and a free trial of 7 days.**

* Stripe subscription would be assigned to the subscriber with**

- Stripe Subscription Attributes
	
	* plan id which is pre created with charge of $49.99

	* payment method= send invoice 

	* Due date for the invoice is assumed to be 10

Customer would be sent an invoice of the membership which would be handled by stripe.


** 3.A User Profile View with Account Details would be available for the customer after login into the account**

** 4.Customer can cancel the membership here (Account Details Page)**

**If the customer cancels the membership **

**Customer's Account State would change on Account details page and also the Stripe subscription will change accordingly**

* Below are the configured Account State Change Status 

	* Active : Customer successfully registers for the membership.

	* Active Canceled : membership would remain active till the end of billing period.

	* Inactive: after the end of billing period.

**Please check through the Screenshots for more details**