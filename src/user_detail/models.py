from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from datetime import timedelta, date
from djstripe.models import Customer
import stripe
import djstripe

class UserProfile(models.Model):
	# user = models.OneToOneField(User,on_delete=models.CASCADE)
	# startDate = models.TextField(blank=True,null=True)
	# free_trial = models.TextField(default='7')
	# freetrial_endDate = models.TextField(blank=True,null=True)

	"""
		Model creates a UserProfile with attributes
			1. user [one to one mapping with the User Model]
			2. startdate = subscription startdate
			3. free_trail = 7
			4. freetrial_endDate = freetrial end date for the customer
 
	"""

	user = models.OneToOneField(User,on_delete=models.CASCADE)
	startDate = models.DateField(null=True)
	free_trial = models.TextField(default='7')
	freetrial_endDate = models.DateField(null=True)



def create_profile(**kwargs):

	"""
		1.Method to create the UserProfile once the customer register
		2.Stripe plan with Id-price_1H6MsvGROzYLWCVNr9xnq3lG has been created with 49.99$ 
		3.Upon customer registration a stripe subscription is created for the customer with the above planId.
		4.Stripe Subscription is created with collection_method = 'send_invoice'.
		5.Stripe will email the customer an invoice with payment instructions for payment.
		6.Days untill due is assumed to be 10.
		7.Free Trial of 7 days is configured in stripe.
	"""

	if kwargs['created']:
		customer,created = Customer.get_or_create(subscriber=kwargs['instance'],livemode=False)
		stripe_subscription = stripe.Subscription.create(
		          customer=customer.id,
		          items=[{"plan": "price_1H6MsvGROzYLWCVNr9xnq3lG"}],
		          collection_method="send_invoice",
		          days_until_due= "10",
		          api_key=djstripe.settings.STRIPE_SECRET_KEY,)

		subscription = djstripe.models.Subscription.sync_from_stripe_data(stripe_subscription)
		user_profile = UserProfile.objects.create(user=kwargs['instance'], 
						startDate=kwargs['instance'].date_joined.date(),
						freetrial_endDate=kwargs['instance'].date_joined.date() + timedelta(days=7))


post_save.connect(create_profile,sender=User)


