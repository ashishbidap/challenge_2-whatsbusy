from django.shortcuts import render
from .models import UserProfile
from .models import User
from djstripe.models import Customer
from datetime import timedelta, date


def user_profile_view(request):
	"""	
		View to for UserProfile of the customer 
		after customer's login or after registeration.

	"""
	id = request.user.id
	obj = UserProfile.objects.get(id=id)
	customer,created = Customer.get_or_create(subscriber=request.user,livemode=False)
	try:
		hasActiveSubscription = customer.active_subscriptions
		statusValue = True
		if hasActiveSubscription == None:
			statusValue = False
		
		if statusValue:	
			invoice = customer.upcoming_invoice()
			context = {

				'user' : obj.user,
				'startDate' : obj.startDate,
				'free_trial' : obj.free_trial,
				'freetrial_endDate' : obj.freetrial_endDate,
				'invoiceDueDate' : invoice.period_end.date(),
				'invoiceAmountDue' : invoice.amount_due,
				'status' : 'Active'
			}
		else:
			context={
			 	 	'user' : obj.user,
					'startDate' : obj.startDate,
					'free_trial' : obj.free_trial,
					'freetrial_endDate' : obj.freetrial_endDate,
					'status' : 'InActive'
			 	}
	except Exception as inst:
		print('err=',inst)
		context={
		 	 	'user' : obj.user,
				'startDate' : obj.startDate,
				'free_trial' : obj.free_trial,
				'freetrial_endDate' : obj.freetrial_endDate,
				'status' : 'Active - Cancelled'
		 	}
	
	return render(request,"userprofile/user_profile_view.html",context)
	

def cancel_subscription(request):

	"""	
		1.Method to cancel the stripe subscription of the customer.
		2.Once the subscription is cancelled the subscription will set to cancel 
		at the end of the billing period.
		
	"""
	customer,created = Customer.get_or_create(subscriber=request.user,livemode=False)
	subscription = customer.subscription
	subscription.cancel(at_period_end=True)
	return render(request,"userprofile/Cancelled_subscription.html")