from django.shortcuts import render,redirect,reverse
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login,logout
from django.contrib.auth.models import User
from django.contrib import messages 



"""
    1.View for handling the login of the customer.
    2.Using Django Authentication System for authenticating,login and logout.
"""
def login_request(request):
    print(request)
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            print("user",user)
            if user is not None:
                login(request, user) 
                return redirect("/userprofile") #redirect the customer to /userprofile after successful login
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request = request,template_name = "login.html",context={"form":form})



# View to handle logout functionality 
def logout_request(request):
    logout(request)
    return redirect("/") #redirect to homepage
