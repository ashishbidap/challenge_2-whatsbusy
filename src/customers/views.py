
from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login,logout
from django.contrib.auth.models import User
from datetime import timedelta, date
from customers.forms import CustomUserCreationForm


def register_view(request,*args,**kwargs):

	"""
		1.Configuring the Registration Page for the Application.
		2.Overriding the existing User Form of Django with additional input fields.

	"""
	if request.method == "POST":
		form = CustomUserCreationForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request,user)
			return redirect("/userprofile") #redirect the customer to /userprofile after customer's successful 
		else:
			for msg in form.error_messages:
				print(form.error_messages[msg])
	form=CustomUserCreationForm
	my_context = { "form" : form}
	return render(request,"customers/user_register.html",my_context)