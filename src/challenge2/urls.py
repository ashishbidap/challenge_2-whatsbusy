from django.contrib import admin
from django.urls import path
from django.contrib.auth import views
from pages.views import login_request,logout_request
from customers.views import register_view
from user_detail.views import user_profile_view,cancel_subscription
from django.conf.urls import include

urlpatterns = [
    
    path('',login_request,name='home'),
    path('home/',login_request),
    path('admin/', admin.site.urls),
    path('register/',register_view,name="register"),
    path('login/',login_request, name="login"),
    path('logout/',logout_request, name="logout"),
    path("userprofile/",user_profile_view,name='userprofile'),
    path("subscription/",cancel_subscription),
]
